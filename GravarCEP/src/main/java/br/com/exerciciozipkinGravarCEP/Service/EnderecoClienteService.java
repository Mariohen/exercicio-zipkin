package br.com.exerciciozipkinGravarCEP.Service;

import br.com.exerciciozipkinGravarCEP.CEP.Cep;
import br.com.exerciciozipkinGravarCEP.CEP.CepUsuario;
import br.com.exerciciozipkinGravarCEP.Model.Endereco;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnderecoClienteService {

    @Autowired
    private CepUsuario cepUsuario;

    public Endereco criar(Endereco endereco) {
        return montarUsuario(endereco, cepUsuario.getByCep(endereco.getCep()));
    }

    private Endereco montarUsuario(Endereco endereco, Cep cep){

            endereco.setCep(cep.getCep());
            endereco.setLogradouro(cep.getLogradouro());
            endereco.setComplemento(cep.getComplemento());
            endereco.setBairro(cep.getBairro());
            endereco.setLocalidade(cep.getLocalidade());
            endereco.setUf(cep.getUf());
            endereco.setUnidade(cep.getUnidade());
            endereco.setIbge(cep.getIbge());
            endereco.setGia(cep.getGia());

            return endereco;
    }

}
