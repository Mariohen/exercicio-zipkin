package br.com.exerciciozipkinGravarCEP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
//@RibbonClients(defaultConfiguration = RibbonConfiguracao.class)
public class GravarCepApplication {

	public static void main(String[] args) {
		SpringApplication.run(GravarCepApplication.class, args);
	}

}
