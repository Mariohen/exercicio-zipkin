package br.com.exerciciozipkinGravarCEP.Controller;

import br.com.exerciciozipkinGravarCEP.Model.Endereco;
import br.com.exerciciozipkinGravarCEP.Service.EnderecoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/gravar")
@ResponseStatus(HttpStatus.CREATED)
public class EnderecoCliente {

    @Autowired
    private EnderecoClienteService enderecoClienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Endereco cadastrarEndereco(@RequestBody Endereco endereco) {
            Endereco objetoEndereco = enderecoClienteService.criar(endereco);

        return objetoEndereco;

    }


}
