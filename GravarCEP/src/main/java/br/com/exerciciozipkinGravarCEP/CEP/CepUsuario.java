package br.com.exerciciozipkinGravarCEP.CEP;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "consulta")
public interface CepUsuario {

    @GetMapping("/consulta/{cep}")
    Cep getByCep(@PathVariable String cep);


}
