package br.com.exerciciozipkinGravarCEP.CEP;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CepUsuarioConfiguracao {

    @Bean
    public ErrorDecoder getCarClientDecoder() {
        return new CepDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CepUsuarioFallBack(), RetryableException.class)
                .withFallbackFactory(CepUsuarioLoadBalanceConfiguracao::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
