package br.com.exerciciozipkinGravarCEP.CEP;

import br.com.exerciciozipkinGravarCEP.Service.CepNotFoundException;

public class CepUsuarioLoadBalanceConfiguracao implements CepUsuario{

    private Exception exception;

    public CepUsuarioLoadBalanceConfiguracao(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Cep getByCep(String cep) {
        if(exception.getCause() instanceof CepNotFoundException) {
            Cep objetoCep = new Cep();
            return objetoCep;
        }
        throw (RuntimeException) exception;
    }
}
