package br.com.exerciciozipkinConsultaCEP.ViaCep;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://viacep.com.br/")
public interface CepCorreios {

    @GetMapping("/ws/{cep}/json/")
    Cep getCep(@PathVariable String cep);

}
