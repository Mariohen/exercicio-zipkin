package br.com.exerciciozipkinConsultaCEP.Controller;

import br.com.exerciciozipkinConsultaCEP.Service.CepService;
import br.com.exerciciozipkinConsultaCEP.ViaCep.Cep;
import br.com.exerciciozipkinConsultaCEP.ViaCep.CepCorreios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consulta")
public class CepController {

    @Autowired
    CepService cepService;

    @Autowired
    CepCorreios cepCorreios;

    @GetMapping("/{cep}")
    public Cep create(@PathVariable String cep) {
        System.out.println("Estou na busca do cep " + cep);
        Cep objetoCep =  cepService.getByCep(cep);
        System.out.println("+++++++++++++++++++++++" + objetoCep.getUf());
        return objetoCep;
    }

}

