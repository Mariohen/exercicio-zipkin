package br.com.exerciciozipkinConsultaCEP.Service;

import br.com.exerciciozipkinConsultaCEP.ViaCep.Cep;
import br.com.exerciciozipkinConsultaCEP.ViaCep.CepCorreios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private CepCorreios cepCorreios;

//    @NewSpan(name = "viacep-service")
//    public Cep getByCep(@SpanTag("cep") String cep) {
//        return cepCorreios.getCep(cep);
//    }

    @NewSpan(name = "viacep-service")
    public Cep getByCep(@SpanTag("cep") String cep) {
        Cep objetoCep = cepCorreios.getCep(cep);
        System.out.println("Estou no servico " + objetoCep.getLocalidade());
        return objetoCep;
    }


}
